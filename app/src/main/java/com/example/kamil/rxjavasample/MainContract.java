package com.example.kamil.rxjavasample;

/**
 * Created by kamil on 25/10/16.
 */

public interface MainContract {
    interface Presenter {
        void load();
    }

    interface View {
        void initializingTask();

        void taskInitialized();

        void startingTask();

        void incrementTaskProgress();

        void taskFinished();
    }
}
