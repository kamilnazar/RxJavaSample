package com.example.kamil.rxjavasample;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kamil on 25/10/16.
 */

class MainActivityPresenter implements MainContract.Presenter {
    private MainContract.View mView;

    MainActivityPresenter(MainContract.View view) {
        mView = view;
    }

    @Override
    public void load() {
        Observable.just(null)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(o -> mView.initializingTask())
                .observeOn(Schedulers.computation())
                .delay(2, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(o -> mView.taskInitialized())
                .doOnNext(o -> mView.startingTask())
                .observeOn(Schedulers.computation())
                .flatMap(o -> doLongRunningJob())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> mView.incrementTaskProgress(),
                        Throwable::printStackTrace,
                        () -> mView.taskFinished());


    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    private Observable<Integer> doLongRunningJob() {
        return Observable.range(1, 10000);
    }
}
