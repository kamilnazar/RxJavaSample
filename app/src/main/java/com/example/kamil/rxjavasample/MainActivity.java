package com.example.kamil.rxjavasample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View {
    private static final String TAG = "MainActivity";
    MainContract.Presenter mPresenter;

    ProgressDialog mIndeterminateProgressDialog, mDeterminateProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mPresenter = new MainActivityPresenter(this);
        initializeProgressDialogs();
    }

    @OnClick(R.id.button)
    public void onClick() {
        mPresenter.load();
    }

    @Override
    public void initializingTask() {
        Log.i(TAG, "initializingTask: "+Thread.currentThread().getName());
        mIndeterminateProgressDialog.show();
    }

    @Override
    public void taskInitialized() {
        Log.i(TAG, "taskInitialized: "+Thread.currentThread().getName());
        mIndeterminateProgressDialog.hide();
    }

    @Override
    public void startingTask() {
        Log.i(TAG, "startingTask: "+Thread.currentThread().getName());
        mDeterminateProgressDialog.setProgress(0);
        mDeterminateProgressDialog.show();
    }

    @Override
    public void incrementTaskProgress() {
        Log.i(TAG, "incrementTaskProgress: "+Thread.currentThread().getName());
        mDeterminateProgressDialog.incrementProgressBy(1);
    }

    @Override
    public void taskFinished() {
        Log.i(TAG, "taskFinished: "+Thread.currentThread().getName());
        mDeterminateProgressDialog.hide();
    }
    private void initializeProgressDialogs(){
        mIndeterminateProgressDialog = new ProgressDialog(this);
        mIndeterminateProgressDialog.setIndeterminate(true);
        mIndeterminateProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mDeterminateProgressDialog = new ProgressDialog(this);
        mDeterminateProgressDialog.setIndeterminate(false);
        mDeterminateProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDeterminateProgressDialog.setMax(10000);
    }
}
